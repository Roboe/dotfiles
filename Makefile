STOW	:= stow --verbose --target=$$HOME


all:
	@$(STOW) --restow bash curl empty-repl-history git nano

delete:
	@$(STOW) --delete */
