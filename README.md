dotfiles
===

Personal dotfiles for my Linux machines.


Installing
---

Clone the repo into your home directory (mind the dot):

    git clone https://gitlab.com/Roboe/dotfiles.git ~/.dotfiles

Ensure you have GNU Stow installed:

    # Debian/Ubuntu
    sudo apt install stow

    # Fedora
    sudo dnf install stow

Then, execute the `Makefile` to create symbolic links:

    cd ~/.dotfiles
    make

If conflicts pop up, backup those files before trying again. E.g.:

    CONFLICTS=".bash_logout .bashrc .profile"
    for DOTFILE in $CONFLICTS; do mv ~/$DOTFILE ~/$DOTFILE.orig; done
