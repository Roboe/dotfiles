# git prompt helpers
if [[ -f /etc/bash_completion.d/git-prompt ]]
then # Debian-based system, git package
  source /etc/bash_completion.d/git-prompt
elif [[ -f /usr/share/git-core/contrib/completion/git-prompt.sh ]]
then # Fedora-based system, git-core package
  source /usr/share/git-core/contrib/completion/git-prompt.sh
elif [[ -f /usr/share/git-core/git-prompt.sh ]]
then # Alpine-based system, git-prompt package
  source /usr/share/git-core/git-prompt.sh
fi
