FLATPAK_PACKAGE_VSCODIUM="com.vscodium.codium"
if flatpak info "$FLATPAK_PACKAGE_VSCODIUM" >/dev/null 2>&1
then
  # shellcheck disable=SC2139 # Expanding here is intended
  alias codium="flatpak run $FLATPAK_PACKAGE_VSCODIUM"
fi

FLATPAK_PACKAGE_CODE_OSS="com.visualstudio.code-oss"
if flatpak info "$FLATPAK_PACKAGE_CODE_OSS" >/dev/null 2>&1
then
  # shellcheck disable=SC2139 # Expanding here is intended
  alias code="flatpak run $FLATPAK_PACKAGE_CODE_OSS"
fi
