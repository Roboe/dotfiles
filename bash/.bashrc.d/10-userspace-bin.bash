# Includes user's bin folder if it exists
LOCAL_BIN="$HOME/.local/bin"
if [[ -x "$LOCAL_BIN" ]]
then
  export PATH="$PATH:$LOCAL_BIN"
fi
