# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Differences between [ and [[: http://mywiki.wooledge.org/BashFAQ/031

# Script indexes:
## 00-09 Dependecies
## 10-19 Session setups
## 20-29 Aliases and helper functions
## 90-99 Graphical output
for SCRIPT in ~/.bashrc.d/*.bash
do source "$SCRIPT"
done
